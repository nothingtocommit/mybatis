package com.example.mybatis;

import org.apache.ibatis.annotations.*;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

@Mapper
public interface RegionMapper {
    @Select("SELECT * from region")
    @Results(value={
        @Result(property="fullName", column="full_name")
    })
    List<RegionModel> findAll();

    @Select("SELECT * FROM region WHERE id=#{regionId}")
    @Results(value={
        @Result(property="fullName", column="full_name")
    })
    RegionModel findById(Long regionId);

    @Insert("INSERT INTO region(full_name, name) VALUES(#{fullName}, #{name})")
    Long save(RegionModel region);

    @Update("UPDATE region SET name=#{region.name},full_name=#{region.fullName} WHERE id=#{id}")
    void update(Long id, RegionModel region);

    @Delete("DELETE FROM region WHERE id=#{regionId}")
    void delete(Long regionId);
}
