package com.example.mybatis;

import org.apache.ibatis.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegionService {
    final RegionMapper regionRepository;

    public RegionService(RegionMapper regionRepository) {
        this.regionRepository = regionRepository;
    }

    public List<RegionModel> findAll() {
        return regionRepository.findAll();
    }

    @Cacheable("regions")
    public RegionModel findById(Long regionId) {
        System.out.println("not cached");
        return regionRepository.findById(regionId);
    }

    @CachePut("regions")
    public RegionModel save(RegionModel region) {
        long regionId = regionRepository.save(region);
        region.setId(regionId);
        return region;
    }
    public void update(Long id, RegionModel region) {
        regionRepository.update(id, region);
    }
    public void delete(Long regionId) {
        regionRepository.delete(regionId);
    }
}
