package com.example.mybatis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class RegionController {
    final RegionService regionService;

    public RegionController(RegionService regionService) {
        this.regionService = regionService;
    }

    @GetMapping("/regions")
    public List<RegionModel> getRegions() {
        return regionService.findAll();
    }

    @GetMapping("/regions/{id}")
    public ResponseEntity<Map<String, Object>> getRegion(@PathVariable Long id) {
        Map<String, Object> responseBody = new HashMap<>();
        HttpStatus httpStatus;
        RegionModel regionModel = regionService.findById(id);
        if (regionModel == null) {
            responseBody.put("error", String.format("Region with an id %s is not found.", id));
            httpStatus = HttpStatus.BAD_REQUEST;
        } else {
            responseBody.put("result", regionModel);
            httpStatus = HttpStatus.OK;
        }
        return new ResponseEntity<>(responseBody, httpStatus);
    }

    @PostMapping("/regions")
    public RegionModel createRegion(@RequestBody RegionModel regionModel) {
        return regionService.save(regionModel);
    }

    @PutMapping("/regions/{id}")
    public ResponseEntity<Map<String, Object>> updateRegion(@RequestBody RegionModel regionModel, @PathVariable Long id) {
        Map<String, Object> responseBody = new HashMap<>();
        HttpStatus httpStatus;
        if (regionService.findById(id) == null) {
            regionModel.setId(id);
            regionService.save(regionModel);
            httpStatus = HttpStatus.CREATED;
        } else {
            regionService.update(id, regionModel);
            httpStatus = HttpStatus.NO_CONTENT;
        }
        return new ResponseEntity<>(responseBody, httpStatus);
    }

    @DeleteMapping("/regions/{id}")
    public void removeRegion(@PathVariable Long id) {
        regionService.delete(id);
    }
}
