create table region (
    id integer not null identity,
    full_name varchar(255) not null,
    name varchar(60) not null,
    primary key(id)
);